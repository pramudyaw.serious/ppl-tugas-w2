#!/bin/bash
echo "start build"
python3 -m pip install -r requirements.txt
python3 manage.py collectstatic --noinput --clear
echo "end build"